from black_jack import Game

# Ввод количества соперников-компьютеров
try:
    players = int(input('Сколько игроков?\n'))
except ValueError:
    players = 1
    print('Введено не целочисленное значение. Заменяем на %d' % players)

while True:
    game = Game(players)
    players = game.players
    if len(players) < 1:
        break


print('Игра окончена.')

print('До новых встреч!')
