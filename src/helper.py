from time import sleep


def prt(str_to_format, *format_args):
    print(str_to_format.format(*format_args))
    sleep(1.5)
