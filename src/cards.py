# -*- coding: utf-8 -*-
import sys


class Card(object):

    types_rank = (
        '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A')

    suit = ''
    type = ''
    name = ''

    def __init__(self, type, suit):
        self.suit = suit
        self.type = type
        self.name = type + '-' + suit

    @staticmethod
    def create_pack():
        pack = []
        for i in range(13):
            pack.append(Card(Card.types_rank[i], 'Heart'))
            pack.append(Card(Card.types_rank[i], 'Shild'))
            pack.append(Card(Card.types_rank[i], 'Sword'))
            pack.append(Card(Card.types_rank[i], 'Blunt'))
        return pack
