import black_jack
import helper


class Player():

    stock = None

    def __init__(self):
        self.stock = []

    def deal(self, card):
        self.stock.append(card)
        helper.prt('{} берет карту {} ({} очков)',
                   self.name, card.name, self.get_points())

    def get_points(self):
        result = 0
        for card in self.stock:
            result += black_jack.Game.get_card_pts(card)
        return result
#        if result > 21 and 11 in self.stock:
#            self.stock[self.stock.index(11)] = 1
#            return self.get_points()

    def isbj(self):
        if len(self.stock) == 2 and self.get_points() == 21:
            helper.prt("{} - Блэк Джек!", self.name)
            return True
        return False


class PlayerHuman(Player):

    name = 'Игрок'
    money = 400
    bet = 0
    ingame = False

    def __init__(self, count=1):
        super().__init__()
        self.name += str(count)

    def set_bet(self):
        inp = input('%s, Ваша ставка?\n' % self.name)
        if inp[0] == '/':  # если при вводе первый символ "/"
            # делим все на то что после символа
            self.bet = self.money // int(inp[1:])
        else:
            self.bet = int(inp)
        if self.bet > self.money:
            self.bet = self.money
        self.money -= self.bet
        helper.prt('{} ставит {}', self.name, self.bet)

    def win(self):
        prize = 2 * self.bet
        self.money += prize
        self.ingame = False
        helper.prt('{} выигрывает {}', self.name, prize)

    def decision(self, game):
        points = self.get_points()
        if points > 21:
            helper.prt('{} перебор!', self.name)
            self.ingame = False
            return
        elif points == 21:
            return
        helper.prt('у Вас {}', points)
        choice = input('Будете брать карту? y/n\n')
        if choice == 'y':
            helper.prt('{} берет карту', self.name)
            self.deal(game.stock.pop())
            self.decision(game)


class PlayerComputer(Player):

    name = 'Компьютер'

    def decision(self, game):
        points = self.get_points()
        if points < 17:
            helper.prt('{} берет карту', self.name)
            self.deal(game.stock.pop())
            self.decision(game)
