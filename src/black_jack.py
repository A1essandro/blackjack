from cards import Card
import helper
from player import *
import random


class Game():

    stock = []
    players = []
    dealer = None
    max_pts = 0

    def __init__(self, players):
        self.stock = Card.create_pack()
        random.shuffle(self.stock)
        self.dealer = PlayerComputer()

        if isinstance(players, int):
            self.add_players(players)
        else:
            self.players = players
            for player in self.players:
                player.stock = []
                player.bet = 0
                player.ingame = True
                helper.prt('{} имеет {}', player.name, player.money)

        self.start_game()
        self.middle_game()
        self.end_game()
        self.check_players_money()

    def add_players(self, num):
        for i in range(1, num + 1):
            player = PlayerHuman(i)
            player.ingame = True
            self.players.append(player)

    def players_in_game(self):
        for player in self.players:
            if player.ingame:
                yield player

    def start_game(self):
        for player in self.players:
            player.set_bet()
        for player in self.players:
            player.deal(self.stock.pop())
            player.deal(self.stock.pop())
        self.dealer.deal(self.stock.pop())
        for player in self.players:
            if player.isbj():
                player.win()

    def middle_game(self):
        for player in self.players_in_game():
            player.decision(self)

    def end_game(self):
        if len(list(self.players_in_game())) == 0:
            return

        self.dealer.decision(self)
        if self.dealer.get_points() > 21:
            self.dealer.stock = []

        for player in self.players_in_game():
            if player.get_points() > self.dealer.get_points():
                player.win()
            elif player.get_points() == self.dealer.get_points():
                player.money += player.bet
                helper.prt('{} остается при своих {}', player.name, player.bet)

    def check_players_money(self):
        for player in self.players:
            if player.money < 1:
                helper.prt(
                    "{} не может продолжать игру без ставок", player.name)
                self.players.remove(player)

    @staticmethod
    def get_card_pts(card):
        if card.type == 'A':
            return 11
        try:
            return int(card.type)
        except ValueError:
            return 10
